﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.IBusiness.IUserModule.Model
{
    /// <summary>
    /// 手机号码登陆
    /// </summary>
    public class LoginByPhoneCode : BaseLogin
    {
        /// <summary>
        /// 手机号
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// 验证码Token
        /// </summary>
        public string token { get; set; }


        /// <summary>
        /// 手机验证码
        /// </summary>
        public int code { get; set; }

    }
}
