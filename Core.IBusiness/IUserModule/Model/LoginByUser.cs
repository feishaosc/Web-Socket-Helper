﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.IBusiness.IUserModule.Model
{
    /// <summary>
    /// 用户名密码登陆
    /// </summary>
    public class LoginByUser : BaseLogin
    {

        /// <summary>
        /// 用户名
        /// </summary>
        public string user { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string password { get; set; }
    }
}
