﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.IBusiness.IUserModule.Model
{
    /// <summary>
    /// 微信登陆
    /// </summary>
    public class LoginByWChat : BaseLogin
    {
        /// <summary>
        /// 微信Token
        /// </summary>
        public string wchat { get; set; }
        
    }
}
