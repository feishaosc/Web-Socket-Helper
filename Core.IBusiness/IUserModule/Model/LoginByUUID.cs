﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.IBusiness.IUserModule.Model
{
    /// <summary>
    /// 根据客户端ID登陆
    /// </summary>
    public class LoginByUUID : BaseLogin
    {
        /// <summary>
        /// UUID
        /// </summary>
        public string uuid { get; set; }
    }
}
