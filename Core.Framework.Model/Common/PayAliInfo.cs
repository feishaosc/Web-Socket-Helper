﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.Common
{
    /// <summary>
    /// 支付宝支付参数
    /// </summary>
    public class PayAliInfo
    {
        /// <summary>
        /// Ali产品ID
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 支付Api服务地址
        /// </summary>
        public string ApiUrl { get; set; }

        /// <summary>
        /// 私钥
        /// </summary>
        public string Privtekey { get; set; }

        /// <summary>
        /// 公钥
        /// </summary>
        public string Publickey { get; set; }
    }
}
