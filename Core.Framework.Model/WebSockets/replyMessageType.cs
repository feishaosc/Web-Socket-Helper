﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.WebSockets
{
    /// <summary>
    /// 回复消息类型
    /// </summary>
    public enum replyMessageType
    {
        被迫下线 = 0,

        系统通知 = 1,

        广播消息 = 2,

        新消息提醒 = 3,

        会话新消息 = 4,

        离线消息 = 5,

        登陆成功 = 6

    }
}
