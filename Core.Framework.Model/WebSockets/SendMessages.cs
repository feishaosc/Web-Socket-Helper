﻿using System;

namespace Core.Framework.Model.WebSockets
{
    /// <summary>
    /// 客户机发送消息
    /// </summary>
    public class SendMessages
    {
        /// <summary>
        /// 会员KEY
        /// </summary>
        public long userKey { get; set; }

        /// <summary>
        /// 会话KEY
        /// </summary>
        public long session { get; set; }

        /// <summary>
        /// 会员名称
        /// </summary>
        public string userName { get; set; }

        /// <summary>
        /// 发送内容
        /// </summary>
        public string sendMessage { get; set; }

        /// <summary>
        /// 发送时间
        /// </summary>
        public DateTime sendTime { get; set; }
    }

}
