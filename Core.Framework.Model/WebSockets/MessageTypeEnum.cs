﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.WebSockets
{
    /// <summary>
    /// 消息类型
    /// </summary>
    public enum MessageTypeEnum
    {
        /// <summary>
        /// 消息只发送给一个订阅客户机
        /// </summary>
        Single = 0,

        /// <summary>
        /// 消息发送给所有订阅客户机
        /// </summary>
        Group = 1
    }
}
