﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.ViewModel
{
    public class ScoketGroupRequest
    {
        public int id { get; set; }
        public string userKey { get; set; }
        public string projectToken { get; set; }
    }
}
