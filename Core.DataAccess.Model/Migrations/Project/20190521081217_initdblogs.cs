﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.DataAccess.Model.Migrations.Project
{
    public partial class initdblogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Config_Dic");

            migrationBuilder.DropTable(
                name: "Config_Project");

            migrationBuilder.DropTable(
                name: "Project_Api");

            migrationBuilder.DropTable(
                name: "Project_Client");

            migrationBuilder.DropTable(
                name: "Project_Custom_Form");

            migrationBuilder.DropTable(
                name: "Project_Custom_Form_List");

            migrationBuilder.DropTable(
                name: "Project_Custom_Form_List_Date");

            migrationBuilder.DropTable(
                name: "Project_DB");

            migrationBuilder.DropTable(
                name: "Project_DB_Tabel");

            migrationBuilder.DropTable(
                name: "Project_DB_Tabel_Copy");

            migrationBuilder.DropTable(
                name: "Project_List");

            migrationBuilder.DropTable(
                name: "Project_Running_Log");

            migrationBuilder.DropTable(
                name: "Project_User");

            migrationBuilder.DropTable(
                name: "Project_User_Api");

            migrationBuilder.DropTable(
                name: "Project_User_Api_Log");

            migrationBuilder.DropTable(
                name: "Project_User_Api_Order");

            migrationBuilder.DropTable(
                name: "Project_WPS");

            migrationBuilder.DropTable(
                name: "Project_WPS_List");

            migrationBuilder.CreateTable(
                name: "Logger",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectToken = table.Column<string>(nullable: true),
                    Template = table.Column<string>(nullable: true),
                    Tag = table.Column<string>(nullable: true),
                    LogFlag = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    AddTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logger", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Logger");

            migrationBuilder.CreateTable(
                name: "Config_Dic",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    title = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    type = table.Column<int>(nullable: true),
                    upKey = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Config_Dic", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Config_Project",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    config = table.Column<string>(unicode: false, nullable: true),
                    projectId = table.Column<int>(nullable: false),
                    token = table.Column<string>(unicode: false, maxLength: 21, nullable: false),
                    type = table.Column<int>(nullable: true),
                    userId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Config_Project", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_Api",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    info = table.Column<string>(unicode: false, maxLength: 350, nullable: false),
                    title = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_Api", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_Client",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Android = table.Column<string>(unicode: false, maxLength: 300, nullable: true),
                    DB = table.Column<string>(unicode: false, maxLength: 150, nullable: true),
                    IOS = table.Column<string>(unicode: false, maxLength: 300, nullable: true),
                    PC = table.Column<string>(unicode: false, maxLength: 300, nullable: true),
                    projectKey = table.Column<int>(nullable: true),
                    WAP = table.Column<string>(unicode: false, maxLength: 300, nullable: true),
                    WEIXIN = table.Column<string>(unicode: false, maxLength: 300, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_Client", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_Custom_Form",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    projectKey = table.Column<int>(nullable: true),
                    title = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_Custom_Form", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_Custom_Form_List",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    customFormKey = table.Column<int>(nullable: true),
                    dateType = table.Column<int>(nullable: true),
                    dateTypeKey = table.Column<int>(nullable: true),
                    def = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    isUsing = table.Column<bool>(nullable: true),
                    title = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_Custom_Form_List", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_Custom_Form_List_Date",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    customFormListKey = table.Column<int>(nullable: true),
                    itemDate = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_Custom_Form_List_Date", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_DB",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    dbName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    passWord = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    projectKey = table.Column<int>(nullable: false),
                    self = table.Column<bool>(nullable: false),
                    sever = table.Column<string>(unicode: false, maxLength: 150, nullable: true),
                    userName = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_DB", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_DB_Tabel",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    colInfoList = table.Column<string>(unicode: false, maxLength: 550, nullable: true),
                    colName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    colType = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    dbKey = table.Column<int>(nullable: false),
                    dec = table.Column<int>(nullable: true),
                    def = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    href = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    info = table.Column<string>(unicode: false, nullable: true),
                    isNull = table.Column<bool>(nullable: true),
                    lenght = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    primaryKry = table.Column<bool>(nullable: false),
                    title = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    upKey = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_DB_Tabel", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_DB_Tabel_Copy",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    colInfoList = table.Column<string>(unicode: false, maxLength: 550, nullable: true),
                    colName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    colType = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    dbKey = table.Column<int>(nullable: false),
                    dec = table.Column<int>(nullable: true),
                    def = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    href = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    info = table.Column<string>(unicode: false, nullable: true),
                    isNull = table.Column<bool>(nullable: true),
                    lenght = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    primaryKry = table.Column<bool>(nullable: false),
                    title = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    upKey = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_DB_Tabel_Copy", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_List",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    Android = table.Column<bool>(nullable: false),
                    info = table.Column<string>(unicode: false, maxLength: 500, nullable: true),
                    IOS = table.Column<bool>(nullable: false),
                    PC = table.Column<bool>(nullable: false),
                    title = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    token = table.Column<string>(unicode: false, maxLength: 21, nullable: true, defaultValueSql: "(right(newid(),(21)))"),
                    userKey = table.Column<int>(nullable: false),
                    WAP = table.Column<bool>(nullable: false),
                    Weixin = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_List", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_Running_Log",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    content = table.Column<string>(unicode: false, nullable: false),
                    LogFlag = table.Column<string>(nullable: true),
                    projectToken = table.Column<string>(unicode: false, maxLength: 21, nullable: false),
                    Tag = table.Column<string>(nullable: true),
                    Templater = table.Column<string>(nullable: true),
                    type = table.Column<int>(nullable: true),
                    userPhone = table.Column<string>(unicode: false, maxLength: 11, nullable: false),
                    userToken = table.Column<string>(unicode: false, maxLength: 21, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_Running_Log", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_User",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    endTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    pass = table.Column<string>(unicode: false, maxLength: 32, nullable: false),
                    phone = table.Column<string>(unicode: false, maxLength: 11, nullable: false),
                    token = table.Column<string>(unicode: false, maxLength: 21, nullable: true, defaultValueSql: "(right(newid(),(21)))"),
                    userName = table.Column<string>(unicode: false, maxLength: 15, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_User", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_User_Api",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    apiCount = table.Column<int>(nullable: true),
                    apiDay = table.Column<int>(nullable: false),
                    apiFreezeCount = table.Column<int>(nullable: true),
                    apiKey = table.Column<int>(nullable: true),
                    userId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_User_Api", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_User_Api_Log",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    apiType = table.Column<int>(nullable: true),
                    content = table.Column<string>(unicode: false, maxLength: 300, nullable: true),
                    execTypeInfo = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    userKey = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_User_Api_Log", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_User_Api_Order",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    apiCount = table.Column<int>(nullable: true, defaultValueSql: "((0))"),
                    apiDay = table.Column<int>(nullable: true, defaultValueSql: "((0))"),
                    apiKey = table.Column<int>(nullable: false),
                    apiMoney = table.Column<decimal>(nullable: true, defaultValueSql: "((0))"),
                    pay = table.Column<bool>(nullable: false),
                    userId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_User_Api_Order", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_WPS",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    projectKey = table.Column<int>(nullable: true),
                    title = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    upKey = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_WPS", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_WPS_List",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    requestHead = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    requestPath = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    requestType = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    retExs = table.Column<string>(unicode: false, nullable: true),
                    sendExs = table.Column<string>(unicode: false, nullable: true),
                    state = table.Column<bool>(nullable: true),
                    title = table.Column<string>(unicode: false, maxLength: 150, nullable: true),
                    wpsKey = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_WPS_List", x => x.id);
                });
        }
    }
}
