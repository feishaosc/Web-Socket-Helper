﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.DataAccess.Model.Migrations
{
    public partial class UpdateAddressUserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "userId",
                table: "Project_Module_Address",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "userId",
                table: "Project_Module_Address",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
