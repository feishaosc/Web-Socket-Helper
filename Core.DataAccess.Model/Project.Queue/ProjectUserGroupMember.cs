﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Core.DataAccess.Model.Project.Queue
{
    [Description("好友关系管理 - 组 - 成员")]
    public class ProjectUserGroupMember
    {
        public int Id { get; set; }
        public int GroupKey { get; set; }
        public string UserId { get; set; }
        public string title { get; set; }
        public string parameter { get; set; }
        public string ProjectToken { get; set; }
        public DateTime? AddTime { get; set; } = DateTime.Now;
    }
}
