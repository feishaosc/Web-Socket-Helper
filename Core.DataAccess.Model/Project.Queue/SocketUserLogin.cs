﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Core.DataAccess.Model.Project.Queue
{
    [Description("记录客户端 - [登陆|登出] 时间")]
    public partial class SocketUserLogin
    {
        public int Id { get; set; }
        public string UserKey { get; set; }
        public string ProjectToken { get; set; }
        public DateTime? LoginTime { get; set; }
        public DateTime? OutTime { get; set; }
    }
}
