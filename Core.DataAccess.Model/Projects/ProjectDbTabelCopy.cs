﻿using System;
using System.Collections.Generic;

namespace Core.DataAccess.Model.Projects
{
    public partial class ProjectDbTabelCopy
    {
        public int Id { get; set; }
        public int DbKey { get; set; }
        public string Title { get; set; }
        public string ColName { get; set; }
        public string ColInfoList { get; set; }
        public string ColType { get; set; }
        public string Info { get; set; }
        public bool? IsNull { get; set; }
        public int? Dec { get; set; }
        public string Def { get; set; }
        public string Lenght { get; set; }
        public bool PrimaryKry { get; set; }
        public int UpKey { get; set; }
        public string Href { get; set; }
        public DateTime AddTime { get; set; }
    }
}
