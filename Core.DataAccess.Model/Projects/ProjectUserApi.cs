﻿using System;
using System.Collections.Generic;

namespace Core.DataAccess.Model.Projects
{
    public partial class ProjectUserApi
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int? ApiKey { get; set; }
        public int? ApiCount { get; set; }
        public int? ApiFreezeCount { get; set; }
        public int ApiDay { get; set; }
        public DateTime? AddTime { get; set; }
    }
}
