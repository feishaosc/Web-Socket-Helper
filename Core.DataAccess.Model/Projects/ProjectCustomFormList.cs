﻿using System;
using System.Collections.Generic;

namespace Core.DataAccess.Model.Projects
{
    public partial class ProjectCustomFormList
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime? AddTime { get; set; }
        public int? DateType { get; set; }
        public string Def { get; set; }
        public bool? IsUsing { get; set; }
        public int? CustomFormKey { get; set; }
        public int? DateTypeKey { get; set; }
    }
}
