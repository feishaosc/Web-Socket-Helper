﻿using System;
using System.Collections.Generic;

namespace Core.DataAccess.Model.Projects
{
    public partial class ProjectUserApiLog
    {
        public int Id { get; set; }
        public int? UserKey { get; set; }
        public int? ApiType { get; set; }
        public string ExecTypeInfo { get; set; }
        public string Content { get; set; }
        public DateTime? AddTime { get; set; }
    }
}
