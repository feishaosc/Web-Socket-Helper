﻿using System;
using System.Collections.Generic;

namespace Core.DataAccess.Model.Projects
{
    public partial class ProjectDb
    {
        public int Id { get; set; }
        public int ProjectKey { get; set; }
        public string Sever { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string DbName { get; set; }
        public bool Self { get; set; }
    }
}
