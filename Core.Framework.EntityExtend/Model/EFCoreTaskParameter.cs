﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;

namespace Core.Framework.EntityExtend.Model
{
    public class EFCoreTaskParameter
    {
        #region Parameter

        public class DictionaryParameter
        {
            public object paramster { get; set; }
            public string sql { get; set; }
        }

        /// <summary>
        /// 当前需处理上文
        /// </summary>
        public static ConcurrentDictionary<DbContext, List<DictionaryParameter>>
            dictionary = new ConcurrentDictionary<DbContext, List<DictionaryParameter>>();

        /// <summary>
        /// 批量插入
        /// </summary>
        public static ConcurrentDictionary<DbContext, List<DataTable>>
            BulkTable = new ConcurrentDictionary<DbContext, List<DataTable>>();

        #endregion
    }
}
