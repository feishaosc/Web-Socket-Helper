﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.EntityExtend.Model
{
    public interface IResult<T>
    {
        string sql { get; set; }
        DbContext context { get; set; }
        List<Tuple<string, object, Type>> Fields { get; set; }
    }
}
