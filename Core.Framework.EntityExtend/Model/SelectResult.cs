﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Core.Framework.EntityExtend.Model
{
    public class SelectResult<T> : ISelectResult<T> 
        where T : class, new()

    {
        public string cacheKey {
            get
            {
                var type = typeof(T);

                return $"{type.Namespace}_{type.Name}_{Skip}_{Take}_{EFCoreCommon.GetWhereStr<T>(where)}";
            }
        }

        /// <summary>
        /// 缓存机制
        /// </summary>
        public IEntityFrameWorkCacheService ICache { get; set; } = null;

        /// <summary>
        /// 是否 刷新缓存
        /// </summary>
        public bool refresh { get; set; }

        /// <summary>
        /// 是否滑动过期（如果在过期时间内有操作，则以当前时间点延长过期时间）
        /// </summary>
        public bool sliding { get; set; }

        public int? Skip { get; set; } = null;

        public int? Take { get; set; } = null;

        public List<string> Includes { get; set; } = null;

        public TimeSpan? TimeSpan { get; set; } = null;

        public IQueryable<T> Model { get; set; }

        /// <summary>
        /// select
        /// </summary>
        public Expression<Func<T, object>> selector { get; set; }

        /// <summary>
        /// order by
        /// </summary>
        public Expression<Func<T, object>> keyasc { get; set; }

        /// <summary>
        /// OrderByDescending
        /// </summary>
        public Expression<Func<T, object>> keydesc { get; set; }

        public Expression<Func<T, bool>> where { get; set; }

    }
}
