
/***  web pc bll  */

(function (it) {


    var parameter = {
        service: 'https://api.itool.store/',
        wss: "wss://api.itool.store/v1/.ws"
    };

    var client = {};
    var WebSocketEve = {
        login: function (call) { },
        offline: function (call) { },
        sesstion: function (call) { }
    };
    var OWebSocket = null;

    /** 请求 */
    var post = function (url, data, call) {
        $.ajax({
            url: url,
            type: "post",
            dataType: "json",
            data: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
            success: call
        });
    };

    var request = {

        /** 获取客户端token */
        GetClientToken: function (call) {
            post(parameter.service + '/.v1/socket/user/.wsbyclient', client, function (e) {
                if (e.code) {
                    client.id = e.date.id;
                    client.token = e.info;
                    call && call(e.info);

                    console.log('sesstion id :'+ client.id)

                } else {
                    console.error(e);
                }
            });
        },

        SendDate: function(params,call){
            post(parameter.service + '/.v1/queue/.push', params, function (e) {
                if (e.code) {
                    call && call(e.info);
                } else {
                    console.error(e);
                }
            });
        }
    };


    it.queue = {
        init: function (params) {

            if (!params) return console.error('iToolQueue params is null');
            if (!params.uuid) return console.error('iToolQueue uuid is null');
            if (!params.projectToken) return console.error('iToolQueue projectToken is null');
            if (!params.parameter) return console.error('iToolQueue parameter is null');

            client = params;
        }
    };

    it.queue.init.prototype = it.queue;


    it.queue.extend = function (obj, prop) {
        if (!prop) {
            prop = obj;
            obj = this;
        }
        for (var o in prop) {
            obj[o] = prop[o];
        }
    };

    it.queue.extend({
        onopen: function (call) {

            if (call) {
                WebSocketEve.onopen = function () {
                    OWebSocket.send("ixpe_userlogin:" + client.token);
                    call && call();
                };

                if (OWebSocket) {
                    OWebSocket.onopen = WebSocketEve.onopen;
                }
            }

        },
        onmessage: function (call) {

            if (call) {
                WebSocketEve.onmessage = function (e) {
                    var msg = JSON.parse(e.data);

                    switch (msg.type) {
                        case 'userlogin':
                            WebSocketEve.login(msg.date);
                            break;

                        case 'offline':
                            WebSocketEve.offline(msg.date);
                            break;

                        case 'clients':
                        case 'length':
                            break;

                        default:
                            msg.messageParameter = JSON.parse(msg.messageParameter);
                            msg.userParameter = JSON.parse(msg.userParameter);
                            WebSocketEve.sesstion(msg);
                            break;
                    }
                    call && call(msg);
                };
                if (OWebSocket) {
                    OWebSocket.onmessage = WebSocketEve.onmessage;
                }
            }

        },
        onclose: function (call) {
            if (call) {
                WebSocketEve.onclose = call;
                if (OWebSocket) {
                    OWebSocket.onclose = call;
                }
            }
        },
        start: function () {
            request.GetClientToken(function (token) {
                OWebSocket = new WebSocket(parameter.wss);
                if (WebSocketEve.onopen) OWebSocket.onopen = WebSocketEve.onopen;
                if (WebSocketEve.onopen) OWebSocket.onmessage = WebSocketEve.onmessage;
                if (WebSocketEve.onopen) OWebSocket.onclose = WebSocketEve.onclose;

            });
        },
        reload: function () {

        },
        login: function (call) {
            if (call) WebSocketEve.login = call;
        },
        offline: function (call) {
            if (call) WebSocketEve.offline = call;
        },
        sesstion: function (call) {
            if (call) WebSocketEve.sesstion = call;
        },


        send: function (params,call) {
            if (!client.token) return console.error('client.token is null');
            if (!params) return console.error('send params is null');
            if (!params.to) return console.error('send tokey is null');
            if (!params.content) return console.error('send content is null');
            if (!params.type) params.type = 'text';
            request.SendDate({
                Content: params.content,
                MessageKey: params.to,
                MessageType: 0,
                Parameter: JSON.stringify({
                    type: params.type
                }),
                ProjectToken: client.projectToken,
                ClientToken: client.token,
                Template: "WebSocket"
            },call);

        }
    });




})(window.iToolQueue = function (params) {
    return new window.iToolQueue.queue.init(params);
});