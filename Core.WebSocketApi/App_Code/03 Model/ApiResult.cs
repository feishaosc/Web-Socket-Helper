﻿using System;

namespace Core.ApiClient.Model
{
    /// <summary>
    /// 接口返回参数标准
    /// </summary>
    public class ApiResult : IResult
    {
        public object date { get; set; }
        public CodeResult code { get; set; } = CodeResult.SUCCESS;

        string _info { get; set; }
        public string info {
            get {
                if (string.IsNullOrWhiteSpace(_info))
                    return code.GetRemark();
                return _info;
            }
            set { _info = value; }
        }
    }
}
