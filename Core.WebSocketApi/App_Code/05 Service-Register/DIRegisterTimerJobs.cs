﻿using System.Threading.Tasks;
using Core.Framework.Util.Common;
using Microsoft.AspNetCore.Builder;
using Quartz;

namespace Core.ApiClient
{

    /// <summary>
    /// 计划任务注册工厂
    /// </summary>
    public class TimerJobsTaskFactory
    {

        private TimerJob job { get; set; }




        public TimerJobsTaskFactory(TimerJob timer)
        {
            this.job = timer;
        }



        public void Execute()
        {

            // 每天定时清理过期用户Token
            this.job.Register<ClearExpiredUserToken>(
                DateBuilder.DateOf(3, 0, 0),
                a => a.WithIntervalInHours(24).RepeatForever()
            );


        }
    }


    /// <summary>
    /// 注册计划任务
    /// </summary>
    public static class DIRegisterTimerJobs
    {
        /// <summary>
        /// 注册使用计划任务
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static void RegisterTimerJobs(this IApplicationBuilder app)
        {

            TimerJob timer = new TimerJob();

            var timerJobsTaskFactory = new TimerJobsTaskFactory(timer);

            timerJobsTaskFactory.Execute();

        }
    }

    

}
