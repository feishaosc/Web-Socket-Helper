﻿using Core.DataAccess.Model.Project.Queue;
using Core.DataAccess.Model.ProjectLogs;
using Core.DataAccess.Model.Projects;
using Core.Framework.Redis;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.ApiClient
{
    public static class DIRegisterContext
    {
        /// <summary>
        /// 注册数据库服务
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddContextService(this IServiceCollection services)
        {
            services.AddTransient<ProjectContext>()
                .AddTransient<ProjectLogsContext>()
                .AddTransient<IRedisHelper,RedisHelper>()
                .AddTransient<ProjectSocketContext>();

            return services;
        }
    }
}
