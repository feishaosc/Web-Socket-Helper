﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.ApiClient
{
    public enum ApiVersions
    {
        v1 = 1,
        v2 = 2,
    }
}
