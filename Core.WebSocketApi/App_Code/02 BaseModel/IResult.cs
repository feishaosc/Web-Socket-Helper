﻿namespace Core.ApiClient
{
    public interface IResult
    {
        /// <summary>
        /// 返回数据
        /// </summary>
        object date { get; set; }

        /// <summary>
        /// 返回状态码
        /// </summary>
        CodeResult code { get; set; }

        /// <summary>
        /// 返回信息
        /// </summary>
        string info { get; set; }
    }
}
