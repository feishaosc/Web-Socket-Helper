﻿using Core.DataAccess.Model.Projects;
using Core.Framework.Util;
using Core.ApiClient.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel;

namespace Core.ApiClient.Controllers.Project
{
    /// <summary>
    /// 项目会员管理
    /// </summary>
    [Route(".v1/project/[controller]")]
    [ApiController]
    public class MemberController : BaseApiController
    {

        /// <summary>
        /// 根据Token获取用户信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IResult Get()
        {
            var result = this.iProjectUser.GetUserInfoByToken(iClientInfo.Token);

            return new ApiResult
            {
                code = result.Item1 ? CodeResult.SUCCESS : CodeResult.DATA_NOT_FOUND,
                date = result.Item2
            };
        }

        /// <summary>
        /// 登陆
        /// </summary>
        /// <param name="model"></param>
        /// <param name="type">登陆类型[.login | .loginByToken]</param>
        /// <returns></returns>
        [HttpPost("{type}")]
        public IResult Login(ProjectUser model,string type)
        {
            Tuple<ProjectUser, bool> result;

            if (type.ToLower().Equals(".login"))
                result = this.iProjectUser.Login(model.Phone, model.Pass, 24 * 3);

            else if (type.ToLower().Equals(".loginByToken".ToLower()))
                result = this.iProjectUser.LoginByToken(model.Token, model.Pass, 24 * 3);

            else
                return Get();

            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.USER_NOT_EXIST,
                date = result.Item1
            };
        }

        /// <summary>
        /// 登陆并设置缓存
        /// </summary>
        /// <param name="model"></param>
        /// <param name="type">登陆类型[.login | .loginByToken]</param>
        /// <param name="hour">身份过期时间</param>
        /// <returns></returns>
        [HttpPost(".{type}.{hour}h")]
        public IResult Login(ProjectUser model, string type, int hour)
        {
            Tuple<ProjectUser, bool> result;

            if (type.ToLower().Equals("login"))
                result = this.iProjectUser.Login(model.Phone, model.Pass, hour);

            else if (type.ToLower().Equals("loginByToken".ToLower()))
                result = this.iProjectUser.LoginByToken(model.Token, model.Pass, hour);

            else
                return Get();

            return new ApiResult
            {
                code = result.Item2 ? CodeResult.SUCCESS : CodeResult.USER_NOT_EXIST,
                date = result.Item1
            };
        }

        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IResult Reg(ProjectUser model)
        {
            var result = this.iProjectUser.Reg(model);

            return new ApiResult
            {
                code = result.Item1 ? CodeResult.SUCCESS : CodeResult.PARAMS_IS_INVALID,
                date = result.Item2,
                info = result.Item3
            };
        }

        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IResult UpdateUserInfo(ProjectUser model)
        {
            var result = this.iProjectUser.UpdateUserInfo(model);
            return new ApiResult
            {
                code = result != null ? CodeResult.SUCCESS : CodeResult.PARAMS_NOT_COMPLETE,
                date = result
            };
        }

        /// <summary>
        /// 退出登陆
        /// </summary>
        /// <param name="token"></param>
        [HttpDelete("{token}")]
        public void OutLogin(string token)
        {
            this.iProjectUser.OutLogin(token);
        }

    }
}
