﻿using Core.Framework.Model.Common;
using Core.Framework.Model.WebSockets;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Middleware.WebSockets
{
    public class RunningLogerInfoModel
    {

        /// <summary>
        /// 当前客户端
        /// </summary>
        public string client { get; set; } = CoreStartupHelper.GetConfigValue("Service:Title");

        /// <summary>
        /// 构造出的消息
        /// </summary>
        public MessageQueue Message { get; set; }

        /// <summary>
        /// 接受到的消息
        /// </summary>
        public QueryMessage TaskMessage { get; set; }

        /// <summary>
        /// 客户端
        /// </summary>
        public List<string> clients { get; set; } = new List<string>();

        /// <summary>
        /// 是否发布
        /// </summary>
        public bool IsPublish { get; set; } = false;
    }
}
